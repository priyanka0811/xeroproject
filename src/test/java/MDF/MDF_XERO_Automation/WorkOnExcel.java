package MDF.MDF_XERO_Automation;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

//import constantValues.ConstantClassValues;


//import constantValues.ConstantClassValues;

public class WorkOnExcel {

         static XSSFWorkbook wb;
         static XSSFSheet ws;
         static XSSFRow rowNumber;
         static XSSFCell cellObject;
         
         
      public static void excelPathMethod(String pathName) throws IOException {
//        	File f = new File(pathName);
//        	FileInputStream fis = new FileInputStream(f);
//        	wb = new XSSFWorkbook(fis);
        	
        	 }
         
         
       public static String readValues_Excel(String pathName, String sheetName, int row, int col) throws IOException {
    	   File f = new File(pathName);
       		FileInputStream fis = new FileInputStream(f);
       		wb = new XSSFWorkbook(fis);
       	
    	   ws = wb.getSheet(sheetName);
    	   
    	   cellObject = ws.getRow(row).getCell(col);
    	   
       if (cellObject.getCellType() == cellObject.CELL_TYPE_NUMERIC) {
		   String intValues = NumberToTextConverter.toText(cellObject.getNumericCellValue());
	       return intValues;
       } else {
    	  
		String cellValue = cellObject.getStringCellValue();
           return cellValue;
		
       }
       
} 
    	   
// To pass values back to excel sheet:   
//      public static void writeValues_Excel(String sheetName, int row, int col, String writeInXLS) throws IOException {
//    	  
//    	  ws = wb.getSheet(sheetName);
//    	  cellObject = ws.getRow(row).getCell(col);
//    	  cellObject.setCellValue(writeInXLS);
//          FileOutputStream fos = new FileOutputStream(new File(ConstantClassValues.excelPath));
//          wb.write(fos);
//          wb.close();
//         
//      }
//         
//     
       
/*       public static void main(String[] args)throws Exception {
    	   System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 1");    	   
  		   excelPathMethod("C:\\New Eclipse Workspaces\\LearningSelenium\\com.dataDriverFramework\\TestCases.xlsx");
  		   //System.out.println(readValues_Excel("TestDataWorksheet", 7, 2));
  		   writeValues_Excel("TestDataWorksheet", 5, 4, "$160");
  		} */
           
}