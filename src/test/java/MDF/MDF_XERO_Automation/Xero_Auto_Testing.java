package MDF.MDF_XERO_Automation;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Xero_Auto_Testing {
	
	static WebDriver driver;
	static ExtentReports report;
	static ExtentTest logger;	
	static String userId;
	static String pwd;
	
	public static void main(String[] args) throws InterruptedException, IOException {	
		
	InitializeDriver();
	
	String fileName = new SimpleDateFormat("'MDF'_yyyyMMddHHmm'.html'").format(new Date());
	String path = "C:\\Mock_Interview_Prep\\August2019\\Extent_Report\\XERO_REPORTS" + fileName ;
	report = new ExtentReports(path);
   
	String excelPath = "C:\\Selenium\\ReadExcel\\MDF_Tekarch.xlsx";
    //WorkOnExcel.excelPathMethod(excelPath);
    String url = WorkOnExcel.readValues_Excel(excelPath, "Login", 0, 0);
    System.out.println(url);
    userId = WorkOnExcel.readValues_Excel(excelPath, "Login", 1, 0);
    pwd = WorkOnExcel.readValues_Excel(excelPath, "Login", 2, 0);
    System.out.println(userId + " " +  pwd);
	//driver.get("https://login.xero.com/");
    driver.get(url);
	//driver.get("https://www.xero.com/us/");
	driver.manage().window().maximize();
	
	defaultLogin();
	//TC_01_A();
	//TC_01_B();
	//TC_01_C();
	//TC_01_D();
	
	//TC_02_A();
	//TC_02_B();
	//TC_02_C();
	//TC_02_D();
	//TC_02_E();
	
	//TC_03_A();
   	//TC_04_A();
    //TC_06_A();
	
	//TC_08_A();
   	//TC_08_B();
	TC_08_C();
   	 
	report.flush();
}

 public static void InitializeDriver() throws IOException {
	 
   String chromeDriverPath = "C:\\Mock_Interview_Prep\\August2019\\JarsAndExes\\chromedriver.exe";
   System.setProperty("webdriver.chrome.driver", chromeDriverPath);
   driver = new ChromeDriver();

//   String fireFoxDriverPath = "C:\\Selenium\\geckodriver-v0.13.0-win64\\geckodriver.exe";
//   System.setProperty("webdriver.gecko.driver",fireFoxDriverPath);
//   driver = new FirefoxDriver();

 }
 public static void defaultLogin() {
	 logger = report.startTest("Login into Xero");
	 driver.findElement(By.id("email")).sendKeys(userId);
	 driver.findElement(By.id("password")).sendKeys(pwd);
	 driver.findElement(By.id("submitButton")).click();
 }
 public static void TC_01_A() {
	 logger = report.startTest("Login into Xero");
	 driver.findElement(By.id("email")).sendKeys(userId);
	 driver.findElement(By.id("password")).sendKeys(pwd);
	 driver.findElement(By.id("submitButton")).click();
	 logger.log(LogStatus.INFO, "Xero website loaded successfully");
	 
 }
 
 public static void TC_01_B() throws IOException {
	 driver.findElement(By.id("email")).sendKeys(userId);
	 driver.findElement(By.id("password")).sendKeys(pwd);
	 driver.findElement(By.id("submitButton")).click();
	 logger.log(LogStatus.FAIL, "Wrong Password entered.Login failed.",logger.addScreenCapture(getScreenShot()));
 }
 
 public static void TC_01_C() throws IOException {
	 driver.findElement(By.id("email")).sendKeys(userId);
	 driver.findElement(By.id("password")).sendKeys(pwd);
	 driver.findElement(By.id("submitButton")).click();
	 logger.log(LogStatus.FAIL, "Wrong Email entered.Login failed.",logger.addScreenCapture(getScreenShot()));
 }
 
 public static void TC_01_D() {
	 driver.findElement(By.xpath("//a[@class='forgot-password-advert']")).click();
	 driver.findElement(By.id("UserName")).sendKeys(userId);
	 driver.findElement(By.xpath("//span[@class='text']")).click();
	 logger.log(LogStatus.INFO, "Forgot Password link open successfully");
 }
 
 public static void TC_02_A() {
	 driver.findElement(By.xpath("//a[contains(text(),'Try Xero for free')]")).click();
	 driver.findElement(By.name("FirstName")).sendKeys("Priyanka");
	 driver.findElement(By.name("LastName")).sendKeys("Saini");
	 driver.findElement(By.name("EmailAddress")).sendKeys("psaini0815@gmail.com");
	 driver.findElement(By.name("PhoneNumber")).sendKeys("6127723450");
	 driver.findElement(By.name("LocationCode")).click();
	 driver.findElement(By.name("TermsAccepted")).click();
	 driver.findElement(By.xpath("//span[@class='g-recaptcha-submit']")).click();
	 //logger.log(LogStatus.INFO, "Account created successfully");
	 
 }
 
 public static void TC_02_B() {
//	 driver.findElement(By.linkText("Free trial")).click();
//	 driver.findElement(By.xpath("//span[@class='g-recaptcha-submit']")).click();
	 
//	 driver.findElement(By.name("EmailAddress")).sendKeys("@gmail.psaini0815.com");//	 
//	 driver.findElement(By.xpath("//span[@class='g-recaptcha-submit']")).click();
	 
	 driver.findElement(By.name("FirstName")).sendKeys("Priyanka");
	 driver.findElement(By.name("LastName")).sendKeys("Saini");
	 driver.findElement(By.name("EmailAddress")).sendKeys("psaini0815@gmail.com");
	 driver.findElement(By.name("PhoneNumber")).sendKeys("6127723450");
	 driver.findElement(By.name("LocationCode")).click();
	 driver.findElement(By.xpath("//span[@class='g-recaptcha-submit']")).click();
}
 
 public static void TC_02_C() {
	 driver.findElement(By.linkText("Free trial")).click();
	 driver.findElement(By.linkText("terms of use")).click();
	 
	 driver.findElement(By.linkText("privacy notice")).click();
}
 
 public static void TC_02_D() {
	 driver.findElement(By.linkText("Free trial")).click();
	 driver.findElement(By.linkText("offer details")).click();
 }
 
 public static void TC_02_E() {
	 driver.findElement(By.linkText("Free trial")).click();
	 driver.findElement(By.linkText("accountant or bookkeeper")).click();
 }
 
 public static void TC_03_A() throws InterruptedException {
	 Thread.sleep(5000);
	 driver.findElement(By.xpath("//a[@class='xrh-focusable xrh-tab--body xrh-tab--body-is-selected']")).click();
	 driver.findElement(By.xpath("//button[contains(text(),'Accounting')]")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.linkText("Reports")).click();
	 driver.findElement(By.xpath("//button[contains(text(),'Contacts')]")).click();
	 driver.findElement(By.xpath("//div[@class='xrh-appbutton--body']")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//a[contains(text(),'Settings')]")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//li[1]//button[1]//div[1]")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//div[@class='xrh-appbutton--body']")).click();	
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//a[contains(text(),'Files')]")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//li[3]//button[1]//div[1]")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//*[@id=\"header\"]/header/div/ol[2]/li[2]/button/div")).click();
	 Thread.sleep(3000);
	 driver.findElement(By.xpath("//li[4]//button[1]//div[1]")).click();
 }
 
 public static void TC_04_A() {
	 driver.findElement(By.xpath("//abbr[@class='xrh-avatar xrh-avatar-color-3']")).click();
	 driver.findElement(By.linkText("Log out")).click();
}
 
 public static void TC_06_A() {
	 driver.findElement(By.xpath("//abbr[@class='xrh-avatar xrh-avatar-color-3']")).click();
	 driver.findElement(By.xpath("//h4[@class='xrh-verticalmenuitem--heading']")).click();
//	 ******** INCOMPLETE ***********
 }
 
 public static void TC_08_A() {
	 
	 driver.findElement(By.xpath("//span[@class='xrh-appbutton--text']")).click();
	 driver.findElement(By.xpath("//a[@class='xrh-verticalmenuitem--body xrh-verticalmenuitem--body-blue']")).click();
	 driver.findElement(By.id("969837f7-bca4-4d4e-9017-53d7a47935c9-control")).sendKeys("Self");
	 driver.findElement(By.xpath("//div[@class='xui-autocompleter--trigger-focus']//div//div//div[@class='xui-iconwrapper xui-iconwrapper-medium']")).sendKeys("PST");
	 driver.findElement(By.id("ade39f1e-02f6-44cc-9931-fc16d9f7ba41-control")).sendKeys("Marketing");
	 driver.findElement(By.linkText("Start trial")).click();
 }
 
 public static void TC_08_B() {
	 driver.findElement(By.xpath("//span[@class='xrh-appbutton--text']")).click();
	 driver.findElement(By.linkText("Add a new organization")).click();
	 driver.findElement(By.id("969837f7-bca4-4d4e-9017-53d7a47935c9-control")).sendKeys("Self");
	 driver.findElement(By.xpath("//div[@class='xui-autocompleter--trigger-focus']//div//div//div[@class='xui-iconwrapper xui-iconwrapper-medium']")).sendKeys("PST");
	 driver.findElement(By.id("ade39f1e-02f6-44cc-9931-fc16d9f7ba41-control")).sendKeys("Marketing");
	 driver.findElement(By.linkText("Buy now")).click();
 }
 
 public static void TC_08_C() {
	 driver.findElement(By.xpath("//span[@class='xrh-appbutton--text']")).click();
	 driver.findElement(By.linkText("Add a new organization")).click();
	 driver.findElement(By.id("7c7cbeb6-8e5e-4431-8d25-97aada02da32-control")).sendKeys("Self");
	 
	 driver.findElement(By.id("ef1c5d08-5bbc-43d0-aade-19ea5526d00a-control")).sendKeys("PST");
	 driver.findElement(By.xpath("//input[@id='10124cf1-116f-41d4-87dc-8dd7fe46bb44-control']")).sendKeys("Accounting");
	 driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[1]/form/div[8]/div/button[2]")).click();
 }
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
	public static String getScreenShot() throws IOException {

		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);

		//after execution, you could see a folder "FailedTestsScreenshots" under src folder
		String destination = "C:\\Selenium\\JarsAndExes\\ExtentReport_jar_files\\XERO_TestReports\\" + "FailedTestsScreenshots\\"+"Screenshot"+dateName+".png";
		System.out.println("Destination is: " + destination);
		File finalDestination = new File(destination);
		FileUtils.copyFile(source,finalDestination);
		return destination;
	}
}


